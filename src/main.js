import Vue from 'vue';
// import 'bootstrap/dist/css/bootstrap-reboot.css';
import element from '@/utils/element';
import '@/style/element.scss';
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap/dist/css/bootstrap-reboot.css';
import '@/utils/quill';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  element,
  render: (h) => h(App),
}).$mount('#app');
