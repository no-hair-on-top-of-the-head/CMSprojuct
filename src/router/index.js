import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView,
  // },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
  // },
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/login',
    component: () => import('@/views/UserLogin.vue'),
  },
  {
    path: '/home',
    component: () => import('@/views/HomePage.vue'),
    redirect: '/home/dashboard',
    children: [
      {
        path: 'article',
        component: () => import('@/views/ArticleManage/ArticleManage.vue'),
      },
      {
        path: 'dashboard',
        component: () => import('@/views/DashBoard.vue'),
      },
    ],
  },
  {
    path: '*',
    component: () => import('@/views/NotFound.vue'),
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  console.log();
  if (to.path === '/login') {
    next();
    return;
  }
  if (store.state.user.token) {
    next();
    return;
  }
  next('/login');
});

export default router;
