import request from '@/utils/request';

export function getArticleList (params) {
  return request.get('admin/interview/query', {
    params,
  });
}
export function updateArticle (update) {
  return request.post('admin/interview/create', update);
}

export function getArticle (id) {
  return request.get('admin/interview/show', {
    params: { id },
  });
}
