import request from '@/utils/request';

export function login (data) {
  return request.post('auth/login', data);
}

export function getUserInfo () {
  return request.get('auth/currentUser');
}

export function logoutUser () {
  return request.get('auth/logout');
}

// export function ActiveUser () {
//   return request.get('analysis/monthlyRetain');
// }
