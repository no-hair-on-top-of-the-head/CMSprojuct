// 导入登录接口
import { login } from '@/api/user';
import { getToken, saveToken, removeToken } from '@/utils/token';

export default {
  // 命名空间
  namespaced: true,
  state: {
    // token赋值
    // token: null,
    token: getToken(),
  },
  getters: {
  },
  mutations: {
    // 存储token的mutation
    saveToken (state, token) {
      state.token = token;
      console.log('vuex的token', state.token);
      // 本地存储
      saveToken(token);
    },
    // 删除token
    removeToken (state) {
      state.token = null;
      // 本地删除token
      removeToken();
    },
  },
  actions: {
    async login (context, user) {
      const res = await login(user);
      context.commit('saveToken', res.token);
    },
    // async ActiveUser () {
    //   const res = await ActiveUser();
    //   console.log(res);
    // },
  },
};
