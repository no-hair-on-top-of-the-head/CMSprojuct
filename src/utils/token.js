const name = 'token';

export function getToken () {
  return localStorage.getItem(name);
}
export function saveToken (token) {
  return localStorage.setItem(name, token);
}
export function removeToken () {
  return localStorage.removeItem(name);
}
