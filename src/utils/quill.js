import Vue from 'vue';
// eslint-disable-next-line import/no-extraneous-dependencies
import VueQuillEditor from 'vue-quill-editor';

// require styles
// eslint-disable-next-line import/no-extraneous-dependencies
import 'quill/dist/quill.core.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'quill/dist/quill.snow.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'quill/dist/quill.bubble.css';

Vue.use(VueQuillEditor /* { default global options } */);
