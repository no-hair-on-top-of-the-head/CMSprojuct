import Vue from 'vue';
import {
  Button, Select, FormItem, Form, Option, OptionGroup, Input, InputNumber, Breadcrumb, BreadcrumbItem, Popconfirm, Pagination, Table, TableColumn, Card, Drawer, Message,
} from 'element-ui';

Vue.use(Button);
Vue.use(Select);
Vue.use(FormItem);
Vue.use(Form);
Vue.use(Option);
Vue.use(OptionGroup);
Vue.use(Input);
Vue.use(InputNumber);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Popconfirm);
Vue.use(Pagination);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Card);
Vue.use(Drawer);
Vue.prototype.$message = Message;

export default Element;
