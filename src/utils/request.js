import axios from 'axios';
// eslint-disable-next-line import/no-cycle
import store from '@/store';
import { Message } from 'element-ui';
import router from '@/router';

axios.defaults.baseURL = 'http://interview-api-t.itheima.net/';

// 添加请求拦截器
axios.interceptors.request.use(
  (config) => {
    console.log('请求对象', config);
    const { token } = store.state.user;
    if (token) {
      // eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error),
);

// 添加响应拦截器
axios.interceptors.response.use(
  (response) => {
    console.log(response);
    return response.data.data;
  },
  (error) => {
    console.log(error);
    if (error.response.status === 401) {
      store.commit('user/removeToken');
      router.push('/login');
    }
    const msg = error.response.data.message;
    if (msg) {
      Message.error(msg);
    }
    return Promise.reject(error);
  },
);

export default axios;
